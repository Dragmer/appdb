//
//  BackendManager.swift
//  AplicacionDB
//
//  Created by Jorge Borja on 23/5/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation

class BackendManager {
    
    func guardarDatosSails(nombre:String,telefono:String,celular:String)
    {
        let urlString = "http://localhost:1337/Contacto/create?nombre=\(nombre)&telefono=\(telefono)&celular=\(celular)"
        
        let url = URL(string: urlString)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error != nil
            {
                print("El error es: \(error)")
                return
            }
            guard let dataResponde = data else {
                print("no hay datos")
                return
            }
            print(dataResponde)
            DispatchQueue.main.async {
                print("Dato ingresado")
            }
        }
        task.resume()
    }
    
}
