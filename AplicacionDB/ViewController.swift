//
//  ViewController.swift
//  AplicacionDB
//
//  Created by Jorge Borja on 23/5/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nombreTextField: UITextField!
    
    @IBOutlet weak var telefonoTextField: UITextField!
    
    @IBOutlet weak var celularTextflied: UITextField!
    
    @IBAction func agregarContactoButton(_ sender: UIButton) {
        
        let nombreContacto = nombreTextField.text ?? "nombre1"
        
        let telefonoContacto = telefonoTextField.text ?? "telefono1"
        
        let celularContacto = celularTextflied.text ?? "celular1"
        
        let bm = BackendManager()
        
        bm.guardarDatosSails(nombre: nombreContacto, telefono: telefonoContacto, celular: celularContacto)
        
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos), name: NSNotification.Name("actualizar"), object: nil)
    }
    
    @IBAction func listarContactoButton(_ sender: UIButton) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func actualizarDatos(_ notification:Notification)
    {
        let clima = (notification.userInfo?["clima"])!
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizar"), object: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "listarSegue"{
            
        }
        
        if segue.identifier == "agregarSegue"{
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

