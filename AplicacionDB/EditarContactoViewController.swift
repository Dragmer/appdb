//
//  EditarContactoViewController.swift
//  AplicacionDB
//
//  Created by Jorge Borja on 23/5/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class EditarContactoViewController: UIViewController {

    
    @IBOutlet weak var nombreEditarTextField: UITextField!
    
    
    @IBOutlet weak var telefonoEditarTextField: UITextField!
    
    
    @IBOutlet weak var celularEditarTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
